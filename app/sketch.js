

var planets = [];
var count = 0;
var earthYears = 0;

var mercury = new Planet({ name: 'mercury', x: 0.39, a: 29,  d: 0.38, r: 91, g: 30, b: 30 });
var venus = new Planet({ name: 'venus', x: 0.72, a: 22, d: 9.5, r: 243, g: 81, b: 0 });
var earth = new Planet({ name: 'earth', x: 1.0, a: 18, d: 1.0, r: 42, g: 166, b: 203 });
var mars = new Planet({ name: 'mars', x: 1.52, a: 15, d: 0.53, r: 228, g: 30, b: 0 });
var jupiter = new Planet({ name: 'jupiter', x: 5.2, a: 8, d: 0.0112, r: 196, g: 129, b: 70 });
var saturn = new Planet({ name: 'saturn', x: 9.58, a: 6, d: 9.45, r: 253, g: 207, b: 167 });
var uranus = new Planet({ name: 'uranus', x: 19.2, a: 3, d: 4.0, r: 78, g: 231, b: 255 });
var neptune = new Planet({ name: 'neptune', x: 30.05, a: 2, d: 3.88, r: 46, g: 80, b: 171 });
planets.push(mercury,venus, earth, mars, jupiter, saturn, uranus, neptune);

var moon = new Moon({name: 'moon', x: 1, a: 1, d: 0.2, r: 217, g: 217, b: 217 });

var sun = {
  x: 0,
  y: 0,
  d: 50
}

function setup() {
  createCanvas(900,900);
  angleMode(DEGREES);
}

function draw() {
  background(0);
  translate(width/2, height/2);

  fill(color('rgb(255, 249, 43)'));

  ellipse(sun.x, sun.y, sun.d);

  translate(sun.x, sun.y);
  push();

  for (var i = 0; i < planets.length; i++){
    var p = planets[i];
    push();
    rotate(p.angle);
    fill(color('rgb(' + p.rgb.red + ',' + p.rgb.green + ',' + p.rgb.blue + ')'));
    p.show();
    p.angle += p.angleChange;
    // reset angle to 0 after full rotation
    if (p.angle > 360){
      p.angle = 0;
      p.rotations++;
      if (p.name == "earth"){
        earthYears++;
      }
    }

    // if current planet is earth, show moon
    if (i == 2){
      translate(p.x, p.y);
      push();
      rotate(moon.angle);
      fill(color('rgb(' + moon.rgb.red + ',' + moon.rgb.green + ',' + moon.rgb.blue + ')'));
      moon.show();
      moon.angle += moon.angleChange;
      pop();
    }
    pop();
  }
  pop();

  // show rotations of each planet
  translate(-width/2, -height/2);
  for (var i = 0; i < planets.length; i++){
    var p = planets[i];
    text(p.name + " rotations: " + p.rotations + ", earth years: " + earthYears, 30,  30 + i*20);
  }
}
