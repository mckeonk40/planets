

let earthDistance = 10;
let earthSpeed = 1;
let earthDiameter = 15;

function Planet({name, x, a, d, r, g, b}){
  this.name = name;
  this.x = x*earthDistance + earthDiameter*8;
  this.y = 0;
  this.angle = 0;
  this.angleChange = a*earthSpeed/15;
  this.diameter = d*earthDiameter;
  console.log(earthDiameter);
  console.log(d*earthDiameter);
  this.rgb = {
    red: r,
    green: g,
    blue: b
  }
  this.rotations = 0;
}

Planet.prototype.show = function () {
  noStroke();
  ellipse(this.x, this.y, this.diameter);
  ellipseMode(CENTER);
  if (this.name == 'saturn'){
    arc(this.x, this.y, 2*earthDiameter, 12*earthDiameter, 0, 360, OPEN);
  }
};
