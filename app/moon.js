// 
// let earthDistance = 10;
// let earthSpeed = 1;
// let earthDiameter = 150;

function Moon({name, x, a, d, r, g, b}){
  // this.pos = createVector(x, 0);
  this.name = name;
  this.x = x*earthDistance + 10;
  this.y = 0;
  this.angle = 0;
  this.angleChange = a*earthSpeed/15;
  this.diameter = d*earthDiameter;
  this.rgb = {
    red: r,
    green: g,
    blue: b
  }
  this.rotations = 0;
}

Moon.prototype.show = function () {
  ellipse(this.x, this.y, this.diameter);
};
